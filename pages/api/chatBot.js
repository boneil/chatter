import cookies from '../../cookie';
const colors = require('colors')

const handler = (req, res) => {
    
    if (req.method === 'POST') {
        handleChatBot(req, res);
    } else {
        // Handle any other HTTP method
    }
    
}

export default cookies(handler)


const crypto = require('crypto');
const jwt = require("jsonwebtoken");
const rp = require("request-promise");
const config = require('../../config');

const WEBCHAT_SECRET = config.get('WEBCHAT_SECRET');
const DIRECTLINE_ENDPOINT_URI = config.get('DIRECTLINE_ENDPOINT_URI');
const APP_SECRET = config.get('APP_SECRET');

console.log(`webchat: ${WEBCHAT_SECRET} app secret: ${APP_SECRET}`);

// // Initialize the web app instance,
// const app = express();
// app.use(cookieParser());
// // Indicate which directory static resources
// // (e.g. stylesheets) should be served from.
// app.use(express.static(path.join(__dirname, "public")));
// // begin listening for requests.
// const port = process.env.PORT || 3000;
// app.listen(port, function () {
//     console.log("Express server listening on port " + port);
// });

function isUserAuthenticated() {
    // add here the logic to verify the user is authenticated
    return true;
}

function handleChatBot(req, res) {
    
    if (!isUserAuthenticated()) {
        res.status(403).send();
        return
    }

    

    const options = {
        method: 'POST',
        uri: 'https://directline.botframework.com/v3/directline/tokens/generate',
        headers: {
            'Authorization': 'Bearer ' + WEBCHAT_SECRET
        },
        json: true
    };
    rp(options)
        .then(function (parsedBody) {
            
            var userid = req.query.userId || req.cookies.userid;
            if (!userid) {
                userid = crypto.randomBytes(4).toString('hex');
                res.cookie("userid", userid);
            }

            var response = {};

            response['userId'] = userid;
            response['userName'] = req.query.userName;
            response['connectorToken'] = parsedBody.token;
            response['optionalAttributes'] = { age: 33 };
            if (req.query.lat && req.query.long) {
                response['location'] = { lat: req.query.lat, long: req.query.long };
            }
            response['directLineURI'] = DIRECTLINE_ENDPOINT_URI;

            console.log(JSON.stringify(response, null, 4));

            const jwtToken = jwt.sign(response, APP_SECRET);
            res.end(JSON.stringify(jwtToken));
        })
        .catch(function (err) {
            res.status(500).end("failed");
            console.error(err);
        });
};
