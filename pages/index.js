import Head from 'next/head'
import React from 'react';


export default function Index() {
    React.useEffect( () => {
        console.log('init chat!')
        chatRequested();
    });
    

    return (
        <React.Fragment>
            <Head>
                    <meta charset="UTF-8" />
                    <title>Health Bot</title>
                    <script src="https://cdn.botframework.com/botframework-webchat/latest/webchat.js"></script>
                    <script src="/chatbot.js"></script>
                    <link href="stylesheets/style.css" rel="stylesheet" />
            </Head>
            <div>
                <div id="webchat" style={{width:"100%"}}></div>
               
            </div>
        </React.Fragment>
    )
}