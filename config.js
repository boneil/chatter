const
    nconf = require('nconf'),
    debug = require('debug')('app:config');

requiredEnvVars = [];

nconf
    .argv()
    .env();

// nconf.argv().env().file({
//     'file': __dirname + '/localConfig.json'
// });

let PORT = 3000;

const config = {
    app: {
        appName: "chatter",
        PORT,
        appRootDir: __dirname,
        WEBCHAT_SECRET: `Od7WcEZjXV4.ftVxCekb8sWn8G8vUZtGLvv9pwI_LcLPd0kYXxcyziw`,
        DIRECTLINE_ENDPOINT_URI: `northamerica.directline.botframework.com`,
        APP_SECRET : `V/wMUlMjb.isZnMFXl0?2YhHNb6G9Pp:`,
    },
    local: {
        
    },
    dev: {
        
    },
    production: {

    }
};

var nodeEnv = nconf.get('NODE_ENV') ? nconf.get('NODE_ENV').toUpperCase() : 'DEV';

if (!nodeEnv) nodeEnv = nconf.get('ENVIRONMENT') ? nconf.get('ENVIRONMENT').toLowerCase() : 'local';

// nconf.file(__dirname + '/localEnv.json');


if (nodeEnv === 'DEV' || nodeEnv === 'DEVELOPMENT') {
    //load up the dev vars
    nconf.defaults(config.dev);

    config.app.runningEnv = nodeEnv || 'DEV';

    console.info('Config loaded running NODE_ENV: ' + '%s'.blue, nodeEnv || '[defaulted to DEV]');
}
else if (nodeEnv === 'PROD' || nodeEnv === 'PRODUCTION') {

    nconf.defaults(config.prod);

    config.app.isProduction = true;

    config.app.runningEnv = nodeEnv;

    console.info('Config loaded running NODE_ENV: %s', nodeEnv);
}
else if (config[nodeEnv]) {
    //load other env like local if we have a node in the config object for it
    nconf.defaults(config[nodeEnv]);
    config.app.runningEnv = nodeEnv;

    debug('loaded config section: %s'.white, nodeEnv);

}
else {
    console.error('unknown environment found: '.bgRed.white + '%s'.inverse.red, nodeEnv);
}

nconf.overrides(config.app);


// extend nconf to get numbers from env
// otherwise return null
nconf.getInteger = function (envVarName) {
    if (isNaN(nconf.get(envVarName))) {
        return null;
    } else {
        return parseInt(nconf.get(envVarName));
    }
};

nconf.getBoolean = function (envVarName) {
    var stringValue = nconf.get(envVarName),
        booleanValue = false;
    if (stringValue === 'true') {
        booleanValue = true;
    } else if (stringValue === null) {
        debug('environment variable ' + envVarName + ' not found');
    } else if (stringValue !== 'false') {
        debug('environment variable ' + envVarName + ' not boolean.  value is ' + stringValue);
    }
    return booleanValue;
};


module.exports = nconf;